# Google Calendar Event Colorizer

A script for google calendar that updates your events with external guests with the predefined color. The external guests are identified by the domain in their e-mail address.

## Getting started

1. Go to https://script.google.com/home
2. Click ‘New script’
3. Paste the script from index.js
4. Read the comments in the top of the index.js file and update the variables to suit your needs.
5. Save the project and give it a name
6. Run the project. It will ask you to give the script permissions to access your calendar.
7. In the triggers tab, add a trigger to run this periodically
